%include "asm_io.inc"

segment .data
    msg1 db "Make your choice : ", 0xA, 0xD
    len1 equ $-msg1
    msg2 db "1 - Rock", 0xA, 0xD
    len2 equ $-msg2
    msg3 db "2 - Paper", 0xA, 0xD
    len3 equ $-msg3
    msg4 db "3 - Scissors", 0xA, 0xD
    len4 equ $-msg4
    compRock db "The Computer entered : Rock", 0xA, 0xD
    len5 equ $-compRock
    compPaper db "The computer entered : Paper", 0xA, 0xD
    len6 equ $-compPaper
    compScissors db "The computer entered : Scissors", 0xA, 0xD
    len7 equ $-compScissors
    winmsg db "You Win !", 0xA, 0xD
    len8 equ $-winmsg
    losemsg db "You Lose !", 0xA, 0xD
    len9 equ $-losemsg
    tiemsg db "It's a tie !", 0xA, 0xD
    len10 equ $-tiemsg
    invalidmsg db "It's not a valid choice !", 0xA, 0xD
    len11 equ $-invalidmsg
    newLine db 0xA, 0xD
    len12 equ $-newLine

segment .bss
    input resd 2
    compChoice resd 1

segment .text
    global asm_main

asm_main:
    enter 0,0
    pusha

beginning:

    ;Comp Choice
    rdtsc
    xor edx, edx
    mov ecx, 3 - 1 + 1
    div ecx
    mov eax, edx
    add eax, 1
    mov [compChoice], eax

    ;Choice prompts
    mov eax, 4
    mov ebx, 1
    mov ecx, msg1
    mov edx, len1
    int 80h
    mov eax, 4
    mov ebx, 1
    mov ecx, newLine
    mov edx, len12
    int 80h

    mov eax, 4
    mov ebx, 1
    mov ecx, msg2
    mov edx, len2
    int 80h
    mov eax, 4
    mov ebx, 1
    mov ecx, msg3
    mov edx, len3
    int 80h
    mov eax, 4
    mov ebx, 1
    mov ecx, msg4
    mov edx, len4
    int 80h

    ;User Input
    mov eax, 3
    mov ebx, 0
    mov ecx, input
    mov edx, 1
    int 80h
    mov ecx, [input]
    sub ecx, '0'
    mov [input], ecx

    ;1st Condition
    mov eax, [input]
    cmp eax, 4
    jz invalid
    js signon
    jno invalid
    jmp seccond

    signon:
        jo invalid
        jmp seccond

    invalid:
        mov eax, 4
        mov ebx, 1
        mov ecx, invalidmsg
        mov edx, len11
        int 80h
        mov eax, 4
        mov ebx, 1
        mov ecx, newLine
        mov edx, len12
        int 80h
        xor eax, eax
        mov [input], eax
        jmp beginning

seccond:
    ;2nd Condition
    mov eax, [input]
    cmp eax, 0
    jz invalid
    js signon2
    jo invalid
    jmp pass

    signon2:
        jo pass
        jmp invalid

pass:
    ;Warp to Result
    mov eax, 4
    mov ebx, 1
    mov ecx, newLine
    mov edx, len12
    int 80h

    mov eax, [compChoice]
    cmp eax, 1
    jz dispRock
    cmp eax, 2
    jz dispPaper
    
    mov eax, 4
    mov ebx, 1
    mov ecx, compScissors
    mov edx, len7
    int 80h
    jmp nxt

    dispRock:
        mov eax, 4
        mov ebx, 1
        mov ecx, compRock
        mov edx, len5 
        int 80h
        jmp nxt

    dispPaper:
        mov eax, 4
        mov ebx, 1
        mov ecx, compPaper
        mov edx, len6
        int 80h
 
nxt:
    ;Test Comp vs Player
    mov eax, [input]
    cmp eax, 1
    jz playRock
    cmp eax, 2
    jz playPaper

    ;Player choosed scissors
    mov ebx, [compChoice]
    cmp ebx, 1
    jz lose
    cmp ebx, 2
    jz win
    jmp tie

playRock:
    mov ebx, [compChoice]
    cmp ebx, 1
    jz tie
    cmp ebx, 2
    jz lose
    jmp win

playPaper:
    mov ebx, [compChoice]
    cmp ebx, 1
    jz win
    cmp ebx, 2
    jz tie
    jmp lose


tie:
    mov eax, 4
    mov ebx, 1
    mov ecx, tiemsg
    mov edx, len10
    int 80h
    jmp exit

win:
    mov eax, 4
    mov ebx, 1
    mov ecx, winmsg
    mov edx, len8
    int 80h
    jmp exit

lose:
    mov eax, 4
    mov ebx, 1
    mov ecx, losemsg
    mov edx, len9
    int 80h
    

exit:
    mov eax, 4
    mov ebx, 1
    mov ecx, newLine
    mov edx, len12
    int 80h
    popa
    mov eax, 0
    leave
    ret
